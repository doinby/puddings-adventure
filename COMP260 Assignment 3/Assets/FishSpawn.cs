﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawn : MonoBehaviour {

    public int nFish = 3;
    public GameObject fishPrefab;
    public float spawnRate = 5f;
    public float xMin = -2.30f;
    public float xMax = 2.30f;

    private GameObject[] fishes;
    private Vector2 fishPos = new Vector2(-35f, -25f);
    private float timeSinceLastSpawned;
    private float spawnYPosition = 7.5f;
    private int currentFish = 0;

	// Use this for initialization
	void Start ()
    {
        SpawnFish();
    }

    void SpawnFish ()
    {
        fishes = new GameObject[nFish];
        for (int i = 0; i < nFish; i++)
        {
            fishes[i] = (GameObject)Instantiate(fishPrefab, fishPos, Quaternion.identity);

            // attach to this object in the hierarchy
            fishes[i].transform.parent = transform;
            // give the bee a name and number
            fishes[i].gameObject.name = "Fish " + i;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        timeSinceLastSpawned += Time.deltaTime;
        
        // if cat 'destroy' all the fishes, make new ones
        if (fishes[currentFish] == null)
        {
            SpawnFish();
        }

        if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0;
            float spawnXPosition = Random.Range(xMin, xMax);
            fishes[currentFish].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentFish++;
            if (currentFish >= nFish)
            {
                currentFish = 0;
            }
        }
    }
}

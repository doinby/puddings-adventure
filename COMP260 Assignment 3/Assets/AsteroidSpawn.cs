﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawn : MonoBehaviour {

    public int nAsteroid = 7;
    public GameObject asteroidPrefab;
    public float spawnRate = 4f;
    public float xMin = -2.30f;
    public float xMax = 2.30f;

    private GameObject[] asteroids;
    private Vector2 asteroidPos = new Vector2(-25f, -15f);
    private float timeSinceLastSpawned;
    private float spawnYPosition = 5.5f;
    private int currentAsteroid = 0;

    // Use this for initialization
    void Start()
    {
        initiateAsteroid();
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;

        // easy difficulty, spawn less often

        if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0;
            float spawnXPosition = Random.Range(xMin, xMax);
            asteroids[currentAsteroid].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentAsteroid++;
            if (currentAsteroid >= nAsteroid)
            {
                currentAsteroid = 0;
            }
        }    

        // hard difficulty, spawn more often
        if (GameControl.instance.score > 5)
        {
            spawnRate = 2f;
            if (GameControl.instance.score > 10)
            {
                spawnRate = 1.5f;
            }
        }      

    }

    void initiateAsteroid ()
    {
            asteroids = new GameObject[nAsteroid];
            for (int i = 0; i < nAsteroid; i++)
            {
                asteroids[i] = (GameObject)Instantiate(asteroidPrefab, asteroidPos, Quaternion.identity);

                // attach to this object in the hierarchy
                asteroids[i].transform.parent = transform;
                // give the bee a name and number
                asteroids[i].gameObject.name = "Asteroid " + i;
            }          
    }
}

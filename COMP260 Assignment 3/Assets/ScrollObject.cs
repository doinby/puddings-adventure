﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollObject : MonoBehaviour {

    private Rigidbody2D rigidbody2d;

	// Use this for initialization
	void Start () {
        rigidbody2d = GetComponent<Rigidbody2D>();
        rigidbody2d.velocity = new Vector2 (0, GameControl.instance.scrollSpeed);

    }
	
	// Update is called once per frame
	void Update () {
        if (GameControl.instance.gameOver == true)
        {
            rigidbody2d.velocity = Vector2.zero;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CatMove : MonoBehaviour {
	public AudioClip FishClip;
	public AudioClip deadFishClip;
	public AudioClip AsteroidClip;
	public AudioClip netClip;
	public AudioClip deadClip;

	private AudioSource audio;

	private bool isDead = false;
	private Rigidbody2D rigidbody2d;
	private Animator catAnimator;

	public Vector2 direction;
    float moveVertical;
    float moveHorizontal;

    void Start()
	{
		audio = GetComponent<AudioSource>();
		rigidbody2d = GetComponent<Rigidbody2D>();
		catAnimator = GetComponent<Animator>();
        
    }

	void Update()
	{
        moveVertical = Input.GetAxis("Vertical");
        moveHorizontal = Input.GetAxis("Horizontal");
        direction = new Vector2(moveHorizontal, moveVertical);

        if (isDead == false)
		{
			rigidbody2d.velocity = direction * GameControl.instance.catSpeed;
		}

        else
        {
            rigidbody2d.velocity = direction * 0;
        }

    }

	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.gameObject.tag == "Obstacle")
		{
			isDead = true;
			catAnimator.SetTrigger("Die");
			GameControl.instance.CatDied();
			audio.clip = AsteroidClip;
			audio.Play();
			//audio.Play(AsteroidClip);
		}

		if (other.gameObject.CompareTag("Net"))
		{
			audio.clip = netClip;
			audio.Play();
			//audio.Play(netClip);
		}
	}
	// void OnCollisionEnter(Collision collision) {
	//audio.PlayOneShot(FishClip);
	//}


	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Fish")) {
			//other.gameObject.SetActive(false);
			Destroy (other.gameObject);
			GameControl.instance.CatScored ();
			audio.clip = FishClip;
			audio.Play();
		}
		if (other.gameObject.CompareTag ("Obstacle")) {
			//other.gameObject.SetActive(false);
			Destroy (other.gameObject);
			isDead = true;
			catAnimator.SetTrigger ("Die");
			GameControl.instance.CatDied ();
			audio.clip = deadFishClip;
			audio.Play();
			//audio.Play(deadFishClip);
		}
	}

}

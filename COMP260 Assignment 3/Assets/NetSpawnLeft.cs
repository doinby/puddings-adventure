﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetSpawnLeft : MonoBehaviour {

    public int nNet = 4;
    public GameObject netPrefab;
    public float spawnRate = 12f;

    public float xMinLeft = -2.2f;
    public float xMaxLeft = -1.7f;

    private GameObject[] nets;
    private Vector2 netPos = new Vector2(-35f, -25f);
    private float timeSinceLastSpawned;
    private float spawnYPosition = 7.5f;
    private int currentNet = 0;

	// Use this for initialization
	void Start ()
    {
        SpawnNet();        
    }

    void SpawnNet()
    {
        nets = new GameObject[nNet];
        for (int i = 0; i < nNet; i++)
        {
            nets[i] = (GameObject)Instantiate(netPrefab, netPos, Quaternion.identity);

            // attach to this object in the hierarchy
            nets[i].transform.parent = transform;
            // give the bee a name and number
            nets[i].gameObject.name = "Net " + i;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        timeSinceLastSpawned += Time.deltaTime;
            if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
            {
                timeSinceLastSpawned = 0;
                float spawnXPosition = Random.Range(xMinLeft, xMaxLeft);
                nets[currentNet].transform.position = new Vector2(spawnXPosition, spawnYPosition);
                currentNet++;
                if (currentNet >= nNet)
                {
                    currentNet = 0;
                }
            }        
    }
}

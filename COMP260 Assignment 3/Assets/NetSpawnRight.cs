﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetSpawnRight : MonoBehaviour {

    public int nNetRight = 6;
    public GameObject netRightPrefab;
    public float spawnRate = 10f;

    private float xMinLeft = 1.87f;
    private float xMaxLeft = 2.20f;

    private GameObject[] nets;
    private Vector2 netPos = new Vector2(-35f, -25f);
    private float timeSinceLastSpawned;
    private float spawnYPosition = 7.5f;
    private int currentNet = 0;

	// Use this for initialization
	void Start ()
    {
        SpawnNet();        
    }

    void SpawnNet()
    {
        nets = new GameObject[nNetRight];
        for (int i = 0; i < nNetRight; i++)
        {
            nets[i] = (GameObject)Instantiate(netRightPrefab, netPos, Quaternion.identity);

            // attach to this object in the hierarchy
            nets[i].transform.parent = transform;
            // give the bee a name and number
            nets[i].gameObject.name = "Net " + i;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        timeSinceLastSpawned += Time.deltaTime;
            if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
            {
                timeSinceLastSpawned = 0;
                float spawnXPosition = Random.Range(xMinLeft, xMaxLeft);
                nets[currentNet].transform.position = new Vector2(spawnXPosition, spawnYPosition);
                currentNet++;
                if (currentNet >= nNetRight)
                {
                    currentNet = 0;
                }
            }        
    }
}

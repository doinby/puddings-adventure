﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMove : MonoBehaviour {

    private Rigidbody2D rigidbody2d;
    public float speed;

    // Use this for initialization
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        speed = Random.Range(-5.0f, -3.0f);
        rigidbody2d.velocity = new Vector2(0, speed);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (GameControl.instance.gameOver == true)
        {
            rigidbody2d.velocity = Vector2.zero;
            transform.Rotate(new Vector3(0, 0, 0));
        }
        else
        {
            transform.Rotate(new Vector3(0, 0, speed));
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {

    private BoxCollider2D groundCollider;
    private float groundVerticalLength;


	// Use this for initialization
	void Start () {
        groundCollider = GetComponent<BoxCollider2D>();
        groundVerticalLength = 17.64f;

    }
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y < -groundVerticalLength)
        {
            RepostionBackground();
        }
	}

    private void RepostionBackground ()
    {
        Vector2 groundOffset = new Vector2 (0, groundVerticalLength);
        transform.position = (Vector2) transform.position + groundOffset;
    }
}

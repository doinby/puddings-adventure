﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetMove : MonoBehaviour {

    private Rigidbody2D rigidbody2d;
    public float netSpeed;

    // Use this for initialization
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        netSpeed = Random.Range(-4.0f, -2.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameControl.instance.gameOver == true)
        {
            rigidbody2d.velocity = Vector2.zero;
            netSpeed = 0.0f;
        }

        else
        {
            rigidbody2d.velocity = new Vector2(0, netSpeed);
        }        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Cat"))
        {
            netSpeed = 0.0f;
        }            
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Cat"))
        {
            netSpeed = -4.0f;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]
public class Menu : MonoBehaviour {

	public GameObject shellPanel;
	public GameObject musicpanel;
	public GameObject guidepanel;
	public Slider voiceslider;
	public Slider musicslider;
	public AudioSource music;
	private bool paused = true;


	void Start () {
        music.volume = 0.2f;

        SetPaused(paused);
		musicpanel.gameObject.SetActive (false);
		guidepanel.gameObject.SetActive (false);
		music.ignoreListenerVolume = true;

		if (PlayerPrefs.HasKey("AudioVolume")) {
			AudioListener.volume = PlayerPrefs.GetFloat("AudioVolume");
		} else {
			
			AudioListener.volume = 1;
		
		}



	}


	void Update () {
		if (!paused && Input.GetKeyDown (KeyCode.Escape)) {
			SetPaused (true);
		}





	}



	private void SetPaused(bool p)
	{

		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;

	}

	public void OnPressedPlay()
	{

		SetPaused(false);
		shellPanel.gameObject.SetActive (false);
	}


	public void OnPressedQuit()
	{
		Application.Quit ();  
	}

	public void OnPressmusic(){

		shellPanel.gameObject.SetActive (false);
		musicpanel.gameObject.SetActive (true);
		voiceslider.value = AudioListener.volume; 
		musicslider.value = music.volume; 
	}

	public void OnPressguide(){

		shellPanel.gameObject.SetActive (false);
		guidepanel.gameObject.SetActive (true);

	}
	public void Applyknew(){
		shellPanel.gameObject.SetActive (true);
		guidepanel.gameObject.SetActive (false);
	}

	public void Applymusic(){
		shellPanel.gameObject.SetActive (true);
		musicpanel.gameObject.SetActive (false);
		AudioListener.volume = voiceslider.value; 
		music.volume = musicslider.value;
		PlayerPrefs.SetFloat ("AudioVolume", AudioListener.volume);

	}


}